#ifndef __BOOSTEX__
#define __BOOSTEX__

#include <boost/filesystem.hpp>

namespace boostex
{
	boost::filesystem::path relativePath(const boost::filesystem::path &path, const boost::filesystem::path &relative_to);
};




boost::filesystem::path boostex::relativePath(const boost::filesystem::path &path, const boost::filesystem::path &relative_to)
{
	// create absolute paths
	auto p = boost::filesystem::absolute(path);
	if (boost::filesystem::is_directory(p) && p.filename() != ".")
		p = p.string() + "/";
	auto r = boost::filesystem::absolute(relative_to);
	if (boost::filesystem::is_directory(r) && r.filename() != ".")
		r = r.string() + "/";
	// if root paths are different, return absolute path
	if (p.root_path() != r.root_path())
		return p;

	// initialize relative path
	boost::filesystem::path result;

	// find out where the two paths diverge
	boost::filesystem::path::const_iterator itr_path = p.begin();
	boost::filesystem::path::const_iterator itr_relative_to = r.begin();
	while (*itr_path == *itr_relative_to && itr_path != p.end() && itr_relative_to != r.end()) {
		++itr_path;
		++itr_relative_to;
	}

	// add "../" for each remaining token in relative_to
	if (itr_relative_to != r.end()) {
		++itr_relative_to;
		while (itr_relative_to != r.end()) {
			result /= "..";
			++itr_relative_to;
		}
	}

	// add remaining path
	while (itr_path != p.end()) {
		result /= *itr_path;
		++itr_path;
	}

	return result;
}

#endif