#include <boost/intrusive_ptr.hpp>

template<class Derived>
class intrusive_ptr_facade
{
public:std::size_t ref_;
private:friend inline void intrusive_ptr_add_ref(Derived* p)
{
			++static_cast<intrusive_ptr_facade<Derived>*>(p)->ref_;
}
		friend inline void intrusive_ptr_release(Derived* p)
		{
			if (--static_cast<intrusive_ptr_facade<Derived>*>(p)->ref_ == 0)
			{
				delete p;
			}
		}

protected:
	intrusive_ptr_facade() : ref_(0) { }
	~intrusive_ptr_facade() { }
};